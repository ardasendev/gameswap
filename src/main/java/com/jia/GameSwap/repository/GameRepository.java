package com.jia.GameSwap.repository;


import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.jia.GameSwap.model.Game;

@Transactional
public interface GameRepository  extends JpaRepository<Game, UUID>
{
	List<Game> findByOrderByPriceAsc();
	
	Optional<Game> findById(UUID id);
	
	//@Query("SELECT COUNT(UserID), game_name FROM GAME GROUP BY game_name")
	//int findOwnersNumber(); // kaç kişi bu oyuna sahip
	
	Game findByName(String name);
	
	List<Game> getByTagName(String tag);
	
	void deleteById(UUID id);
	
	
	//@Query("SELECT NAME FROM category WHERE category = :category)
	//Game findByCategory(String category);
}
