package com.jia.GameSwap.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.jia.GameSwap.model.Game;
import com.jia.GameSwap.model.Library;

@Transactional
public interface LibraryRepository extends JpaRepository<Library, UUID>
{
	//List<Library> findAll();
	
	//public Library getByCode();
	
	public List<Library> getByUserId(UUID userId);
	
	/*@Query("SELECT game FROM library WHERE library.userId = :id")
	List<Game> getGamesByUserId(UUID id);*/
}
