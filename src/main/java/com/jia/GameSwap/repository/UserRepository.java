package com.jia.GameSwap.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.jia.GameSwap.core.DataResult;
import com.jia.GameSwap.core.Result;
import com.jia.GameSwap.model.Game;
import com.jia.GameSwap.model.User;

@Transactional
public interface UserRepository extends JpaRepository<User, UUID>
{
	Optional<User> findById(UUID id);
	
	public User getByNickName(String nickName);
	
	public User getByName(String name);
	
	public User getBylastName(String lastName);
	
	void deleteBynickName(String nickName);
	
	void deleteById(UUID id);
}
