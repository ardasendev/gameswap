package com.jia.GameSwap.service;

import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.management.RuntimeErrorException;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.jia.GameSwap.core.DataResult;
import com.jia.GameSwap.core.Result;
import com.jia.GameSwap.core.SuccessDataResult;
import com.jia.GameSwap.core.SuccessResult;
import com.jia.GameSwap.dto.GameDTO;
import com.jia.GameSwap.model.Game;
import com.jia.GameSwap.repository.GameRepository;

import ch.qos.logback.core.joran.util.beans.BeanUtil;
@Service
public class GameServiceImp implements GameService {

	@Autowired
	private GameRepository gameRepository;

	@Override
	public DataResult<List<Game>> getGameList() 
	{
		return new SuccessDataResult<List<Game>>(gameRepository.findAll(),"Oyun Listesi döndürüldü.");
	}

	@Override
	public DataResult<Game> getGame(UUID id) {
		
		return new SuccessDataResult<Game>(gameRepository.findById(id).get(),"ID'ye göre kullanıcı döndürüldü.");
	}

	@Override
	public Result saveGame(GameDTO gameDTO) 
	{
		Game game = new Game();
		game.setId(UUID.randomUUID());
		game.setCreateDate(Calendar.getInstance());
		BeanUtils.copyProperties(gameDTO, game);
		gameRepository.save(game);
		return new SuccessResult("Oyun eklendi.");
	}
			
	@Override
	public Result deleteGameById(UUID id) 
	{
		Game game = gameRepository.findById(id).get();
		if(Objects.isNull(game)) {
			throw new RuntimeErrorException(null,"GameID mevcut değil");
		}
		gameRepository.delete(game);
		return new SuccessResult("ID'si girilen oyun silindi.");
	}


	@Override
	public Result getGameIdAddTags(UUID id, List<String> tags) 
	{
		Game newGame = gameRepository.findById(id).get(); 
		newGame.setTagName(tags);
		gameRepository.save(newGame);
		
		return new SuccessResult("ID'si verilen oyuna tag eklendi.");
	}

	@Override
	public Result deleteAllGames() 
	{
		gameRepository.deleteAll();
		return new SuccessResult("Tüm oyunlar silindi");
	}

	@Override
	public DataResult<List<Game>> filterTagGame(String tag) {
		
		
		return new SuccessDataResult<List<Game>>(gameRepository.getByTagName(tag),"Tag girilen oyunlar döndürüldü.");
	}

	@Override
	public Result updateGame(UUID id, GameDTO gameDTO) {
		
		Game newGame = gameRepository.getById(id);
		newGame.setName(gameDTO.getName());
		newGame.setPrice(gameDTO.getPrice());
		newGame.setTagName(gameDTO.getTagName());
		gameRepository.save(newGame);
		return new SuccessResult("Oyun bilgisi güncellendi.");
		
	}

	@Override
	public DataResult<List<Game>> sortListGames(String type, String value) 
	{
		Sort sort = null;
		
		switch (value) 
		{
		case "price":
			if(type.equalsIgnoreCase("ASC"))
			{
				sort = Sort.by(Sort.Direction.ASC,"price");
			}
			else if(type.equalsIgnoreCase("DESC"))
			{
				sort = Sort.by(Sort.Direction.DESC,"price");
			}
			break;
			
		case "name":
			if(type.equalsIgnoreCase("ASC"))
			{
				sort = Sort.by(Sort.Direction.ASC,"name");
			}
			else if(type.equalsIgnoreCase("DESC"))
			{
				sort = Sort.by(Sort.Direction.DESC,"name");
			}
			break;
			
		case "createDate":
			if(type.equalsIgnoreCase("ASC"))
			{
				sort = Sort.by(Sort.Direction.ASC,"createDate");
			}
			else if(type.equalsIgnoreCase("DESC"))
			{
				sort = Sort.by(Sort.Direction.DESC,"createDate");
			}
			break;

		default:
			sort = Sort.by(Sort.Direction.ASC,"name");
			break;
		}
		
		return new SuccessDataResult<List<Game>>(gameRepository.findAll(sort),"Verilen bilgilere göre sıralama yapıldı.");
	}

}
