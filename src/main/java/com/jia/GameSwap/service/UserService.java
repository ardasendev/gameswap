package com.jia.GameSwap.service;

import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Service;

import com.jia.GameSwap.core.DataResult;
import com.jia.GameSwap.core.Result;
import com.jia.GameSwap.dto.UserDTO;
import com.jia.GameSwap.model.User;

@Service
public interface UserService 
{
	
	public DataResult<User> getUserById(UUID id);
	
	public DataResult<User> getUserByNickName(String nickName);

	public Result saveUser(UserDTO userDTO);

	public Result updateUser(String nickName, UserDTO userDTO);
	
	public Result deleteUserById(String id);
	
	public Result deleteUserByNickName(String nickName);
	
	public Result deleteAllUser();
	
	public Result updateUserPassword(UUID id, String password);
	
	public DataResult<Double> getUserByCoins(String nickName, Double coin);

	public DataResult<List<User>> getUsersList();
	
	
	
}
