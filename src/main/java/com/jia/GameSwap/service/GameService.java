package com.jia.GameSwap.service;

import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Service;

import com.jia.GameSwap.core.DataResult;
import com.jia.GameSwap.core.Result;
import com.jia.GameSwap.dto.GameDTO;
import com.jia.GameSwap.model.Game;

@Service
public interface GameService 
{

	public DataResult<Game> getGame(UUID id);

	public Result saveGame(GameDTO gameDTO);
	
	public Result updateGame(UUID id, GameDTO gameDTO);

	public Result getGameIdAddTags(UUID id, List<String> tags);
	
	public Result deleteGameById(UUID id);
	
	public Result deleteAllGames();
	
	public DataResult<List<Game>> getGameList();
	
	public DataResult<List<Game>> sortListGames(String type, String value);
	
	public DataResult<List<Game>> filterTagGame(String tag);

	
	
}
