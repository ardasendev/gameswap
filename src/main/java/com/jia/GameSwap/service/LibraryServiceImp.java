package com.jia.GameSwap.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.management.RuntimeErrorException;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jia.GameSwap.core.DataResult;
import com.jia.GameSwap.core.Result;
import com.jia.GameSwap.core.SuccessDataResult;
import com.jia.GameSwap.core.SuccessResult;
import com.jia.GameSwap.dto.LibraryDTO;
import com.jia.GameSwap.model.Game;
import com.jia.GameSwap.model.Library;
import com.jia.GameSwap.model.User;
import com.jia.GameSwap.repository.GameRepository;
import com.jia.GameSwap.repository.LibraryRepository;
import com.jia.GameSwap.repository.UserRepository;

@Service
public class LibraryServiceImp implements LibraryService
{
	@Autowired
	private LibraryRepository libraryRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private GameRepository gameRepository;
	
	@Override
	public Result saveLibrary(String userId, String gameId) 
	{
		User user = userRepository.findById(UUID.fromString(userId)).get();
		Game game = gameRepository.findById(UUID.fromString(gameId)).get();
		Library library = new Library(UUID.randomUUID(), user.getId(), game.getId(), UUID.randomUUID()+game.getName());
		libraryRepository.save(library);
		return new SuccessResult("Oyun kütüphaneye eklendi.");
	}

	@Override
	public DataResult<List<Library>> getLibraryList() {
		return new SuccessDataResult<List<Library>>(libraryRepository.findAll(),"Kütüphaneler listelendi.");
	}

	@Override
	public DataResult<List<LibraryDTO>> getGamesByNickName(String nickName) {
		User user = userRepository.getByNickName(nickName);
		
		if(Objects.isNull(user)) {
			throw new RuntimeErrorException(null,"nickName mevcut değil");
		}
		List<LibraryDTO> libraryListDTO = new ArrayList<>();
		List<Library> libraryList = libraryRepository.getByUserId(user.getId());
		for (Library lib : libraryList) {
			LibraryDTO libraryDTO = new LibraryDTO();
			Game game = gameRepository.findById(lib.getGameId()).get();
			libraryDTO.setGameName(game.getName());
			libraryDTO.setUserNickName(user.getNickName());
			libraryDTO.setCode(libraryDTO.getCode());
			libraryListDTO.add(libraryDTO);
		}
		
		return new SuccessDataResult<List<LibraryDTO>>(libraryListDTO,"Nickname ile oyunlar döndürüldü.");
	}

	
	
}
