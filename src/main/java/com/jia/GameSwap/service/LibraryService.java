package com.jia.GameSwap.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.jia.GameSwap.core.DataResult;
import com.jia.GameSwap.core.Result;
import com.jia.GameSwap.dto.LibraryDTO;
import com.jia.GameSwap.model.Library;

@Service
public interface LibraryService
{

	Result saveLibrary(String userId, String gameId);

	DataResult<List<Library>> getLibraryList();


	DataResult<List<LibraryDTO>> getGamesByNickName(String nickName);
	
}
