package com.jia.GameSwap.service;

import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import javax.management.RuntimeErrorException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jia.GameSwap.core.DataResult;
import com.jia.GameSwap.core.Result;
import com.jia.GameSwap.core.SuccessDataResult;
import com.jia.GameSwap.core.SuccessResult;
import com.jia.GameSwap.dto.UserDTO;
import com.jia.GameSwap.model.Game;
import com.jia.GameSwap.model.User;
import com.jia.GameSwap.repository.UserRepository;
@Service
public class UserServiceImp implements UserService
{
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public DataResult<List<User>> getUsersList() 
	{
		return new SuccessDataResult<List<User>>(userRepository.findAll(),"Kullanıcı listesi döndürüldü.");
	}

	@Override
	public DataResult<User> getUserByNickName(String nickName) 
	{
		return new SuccessDataResult<User>(userRepository.getByNickName(nickName),"Nickname ile kullanıcı döndürüldü.");
	}

	@Override
	public Result saveUser(UserDTO userDTO) 
	{
		User user = new User();
		user.setName(userDTO.getName());
		user.setLastName(userDTO.getLastName());
		user.setCoin(0.0);
		user.setCreateDate(Calendar.getInstance());
		user.setNickName(userDTO.getNickName());
		user.setPassword(userDTO.getPassword());
		user.setId(UUID.randomUUID());
		userRepository.save(user);
		return new SuccessResult("Kullanıcı eklendi.");
		
	}

	@Override
	public Result updateUser(String nickName, UserDTO userDTO) {
		User newUser = userRepository.getByNickName(nickName);
		newUser.setPassword(userDTO.getPassword());
		userRepository.save(newUser);
		return new SuccessResult("Kullanıcı bilgileri güncellendi.");
	}

	@Override
	public DataResult<User> getUserById(UUID id) {
		
		return new SuccessDataResult<User>(userRepository.findById(id).get(),"ID ile kullanıcı döndürüldü.");
	}

	@Override
	public Result deleteUserById(String id) {
		UUID pathId = UUID.fromString(id);
		Optional<User> user = userRepository.findById(pathId);
		if(!user.isPresent()) {
			throw new RuntimeErrorException(null,"UserID mevcut değil");
		}
		userRepository.delete(user.get());
		return new SuccessResult("ID girilen kullanıcı silindi.");
		
	}

	@Override
	public Result deleteUserByNickName(String nickName) {
		userRepository.deleteBynickName(nickName);
		return new SuccessResult("Nickname girilen kullanıcı silindi.");
		
	}

	@Override
	public Result deleteAllUser() {
		userRepository.deleteAll();
		return new SuccessResult("Tüm kullanıcılar silindi.");
		
	}

	@Override
	public Result updateUserPassword(UUID id , String password) {
		User newUser = userRepository.findById(id).get();
		newUser.setPassword(password);
		userRepository.save(newUser);
		return new SuccessResult("Kullanıcının şifresi değiştirildi.");
		
	}

	@Override  //Hata olabilir.
	public DataResult<Double> getUserByCoins(String nickName, Double coin) {
		User newUser = userRepository.getByNickName(nickName);
		double amount = newUser.getCoin();
		return new SuccessDataResult<Double>(amount,"Coin bilgisi döndürüldü.");
		
		
	}

	

}
