package com.jia.GameSwap.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LibraryDTO 
{
	private String gameName;
	
	private String UserNickName;
	
	private String code;
}
