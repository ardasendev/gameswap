package com.jia.GameSwap.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO 
{
	private String nickName;
	
	private String password;
	
	private String name;

	private String lastName;

}
