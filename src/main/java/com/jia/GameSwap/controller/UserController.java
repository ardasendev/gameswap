package com.jia.GameSwap.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jia.GameSwap.core.DataResult;
import com.jia.GameSwap.core.Result;
import com.jia.GameSwap.core.SuccessDataResult;
import com.jia.GameSwap.dto.GameDTO;
import com.jia.GameSwap.dto.UserDTO;
import com.jia.GameSwap.model.Game;
import com.jia.GameSwap.model.User;
import com.jia.GameSwap.service.UserService;

@RestController
@RequestMapping("/api/user")
public class UserController 
{
	@Autowired
	private UserService userService;
	
	@GetMapping("/all")
	public DataResult<List<User>> getListUser() 
	{
		return userService.getUsersList();
	}
	
	@GetMapping("/{username}")
	public DataResult<User> getUser(@PathVariable("username") String nickName) 
	{
		return userService.getUserByNickName(nickName);
	}
	
	@PostMapping("/add")
	public Result addUser(@RequestBody UserDTO userDTO) 
	{
		return userService.saveUser(userDTO);
	}
	
	@PostMapping("/{username}/update-profile")
	public Result updateUser(@PathVariable("username") String nickName, @RequestBody UserDTO userDTO) {
		return userService.updateUser(nickName,userDTO);
	}
	
	@GetMapping("/getuser/{user-id}")
	public DataResult<User> getUserById(@PathVariable("user-id") UUID id) {
		return userService.getUserById(id);
	}
	
	@DeleteMapping("/delete/{user-id}")
	public Result deleteUserById(@PathVariable("user-id") String id) {
		return userService.deleteUserById(id);
	}
	
	@DeleteMapping("/deletenickname/{nickname}")
	public Result deleteUserByNickname(@PathVariable("nickname") String nickname) {
		return userService.deleteUserByNickName(nickname);
	}
	
	@DeleteMapping("/allDelete")
	public Result deleteAllUser() {
		return userService.deleteAllUser();
	}
	
	@PostMapping("/{id}/password-update")
	public Result updatePassword(@PathVariable("id") UUID id, @RequestBody String password) {
		return userService.updateUserPassword(id,password);
	}
	
	
}
