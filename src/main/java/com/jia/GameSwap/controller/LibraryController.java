package com.jia.GameSwap.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jia.GameSwap.core.DataResult;
import com.jia.GameSwap.core.Result;
import com.jia.GameSwap.dto.LibraryDTO;
import com.jia.GameSwap.model.Game;
import com.jia.GameSwap.model.Library;
import com.jia.GameSwap.model.User;
import com.jia.GameSwap.service.GameService;
import com.jia.GameSwap.service.LibraryService;
import com.jia.GameSwap.service.UserService;

@RestController
@RequestMapping("/api/lib")
public class LibraryController 
{

	@Autowired
	private LibraryService libraryService;
	
	@PostMapping("/add")
	public Result userAddGame(String userId, String gameId) 
	{
		return libraryService.saveLibrary(userId, gameId);
	}
	
	@GetMapping("/all")
	public DataResult<List<Library>> getLibraryList(){
		return libraryService.getLibraryList();
	}
	
	@GetMapping("/{nickname}")
	public DataResult<List<LibraryDTO>> getGamesByNickName(@PathVariable("nickname") String nickName) {
		return libraryService.getGamesByNickName(nickName);
	}
	
}
