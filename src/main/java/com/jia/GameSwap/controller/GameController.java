package com.jia.GameSwap.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jia.GameSwap.core.DataResult;
import com.jia.GameSwap.core.Result;
import com.jia.GameSwap.dto.GameDTO;
import com.jia.GameSwap.model.Game;
import com.jia.GameSwap.repository.GameRepository;
import com.jia.GameSwap.service.GameService;

@RestController
@RequestMapping("/api/game")
public class GameController 
{
	
	@Autowired
	private GameService gameService;
	
	@GetMapping("/all")
	public DataResult<List<Game>> getGameList()
	{
		return gameService.getGameList();
	}
	
	@GetMapping("/{game-id}")
	public DataResult<Game> getGames(@PathVariable("game-id") UUID id) 
	{
		return gameService.getGame(id);
	}
	
	@PostMapping("/add")
	public Result addGame(@RequestBody GameDTO gameDTO) 
	{
		return gameService.saveGame(gameDTO);
	}
	
	@PostMapping("/{game-id}/add-tags")
	public Result getGameIdAddTags(@PathVariable("game-id") UUID id,@RequestBody List<String> tags) 
	{
		return gameService.getGameIdAddTags(id,tags);
	}
	
	@PostMapping("/{game-id}/update")
	public Result updateGame(@PathVariable("game-id") UUID id, @RequestBody GameDTO gameDTO)
	{
		return gameService.updateGame(id,gameDTO);
	}
	
	@DeleteMapping("/delete/{game-id}")
	public Result deleteGame(@PathVariable("game-id") UUID id) 
	{
		return gameService.deleteGameById(id);
	}
	
	@DeleteMapping("/delete-all")
	public Result deleteAllGames() {
		return  gameService.deleteAllGames();
	}
	
	@GetMapping("/sortList/{sort-type}/{value}")
	public DataResult<List<Game>> sortList(@PathVariable("sort-type") String sortType,@PathVariable("value") String value)
	{
		return gameService.sortListGames(sortType,value);
	}
	
	@GetMapping("/filter/{tag}")
	public DataResult<List<Game>> filterTagGame(@PathVariable("tag") String tag){
		return gameService.filterTagGame(tag);
		
	}
	

}
