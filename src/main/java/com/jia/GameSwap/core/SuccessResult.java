package com.jia.GameSwap.core;

public class SuccessResult extends Result
{
	public SuccessResult() 
	{
		super(true);
	}
	
	public SuccessResult(String mesaj) 
	{
		super(true,mesaj);
	}

}
