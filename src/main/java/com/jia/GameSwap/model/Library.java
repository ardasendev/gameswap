package com.jia.GameSwap.model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Library 
{
	@Id
	@Column(name = "library_id")
	private UUID id;
	
	@Column(name = "user_id")
	private UUID userId;
	
	@Column(name = "game_id")
	private UUID gameId;
	
	@Column(name = "game_code")
	private String code;
}
