package com.jia.GameSwap.model;

import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Game {
	
	@Id
	@Column(name = "game_id")
	private UUID id;
	
	@Column(name= "game_name")
	private String name;
	
	@Column(name= "game_price")
	private Double price;
	
	@Column(name="game_create_date")
	private Calendar createDate;
	
	@ElementCollection
	@CollectionTable(name = "game_tags", joinColumns = @JoinColumn(name = "game_id"))
	private List<String> tagName;
	
	

}
